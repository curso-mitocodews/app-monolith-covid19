# Aplicación Monolítica
_Este proyecto es una aplicación de tipo Monolótica para el curso Arquitecto DevOps - mitocode._


## Objetivo
_Este proyecto tiene como finalidad aplicar lo aprendido en el curso, mediante la implementación de Integracón continua, Entrega continua y Despliegue Continuo_


## Pre-requisitos
_Se requerie acceso a:_

* [Jenkins](http://208.68.39.62:8080/)
* [Sonarqube](http://208.68.39.62:9000/projects)
* [JFrog Artifactory](http://208.68.39.62:8082/ui/)
* [Jboss](http://167.99.144.193:9990/)(opcional)

_Repositorio Público_

* [Bitbucket](https://bitbucket.org/curso-mitocodews/app-monolith-covid19)


## Ejecutar pipeline
_Para poder ejecutar el job, se debe ingresar al servidor de_ [Jenkins](http://208.68.39.62:8080/)_, luego al workspace de curso-mitocodews, 
y selecionar la rama que corresponda y por último dar clic en la opción construir ahora._

_Fases:_

* Build
* Testing
* Sonar 
* Artifactory 
* Despliegue en Jboss 7.2


## Artifactory
_Para el alojamiento de los artefactos en _[JFrog Artifactory](http://208.68.39.62:8082/ui/)_, se maneja la siguiente lógica._
_La rama master se alojará en el repositorio _[release](http://208.68.39.62:8082/ui/repos/tree/General/app-monolith-covid19-release)
```
Ejemplo:
http://208.68.39.62:8082/artifactory/app-monolith-covid19-release/com/app-monolith-covid19/1.0.0/covid19-0.0.1-SNAPSHOT.war
```
_Para otras ramas se alojará en el repositorio _[snapshot](http://208.68.39.62:8082/ui/repos/tree/General/app-monolith-covid19-snapshot)_ concatenado el número del job._
```
Ejemplo:
http://208.68.39.62:8082/artifactory/app-monolith-covid19-snapshot/com/app-monolith-covid19/1.0.0-SNAPSHOT-1
```


## Sonarqube
_Para visualizar el reporte, se debe ingresar a_ [Sonarqube](http://208.68.39.62:9000/dashboard?id=com.glam%3Acovid19)

_También se puede observar la cobertura  en jenkins en la sección tendencia de cobertura._


## Jboss 
_Si se requiere validar el artefacto desplegado se debe de ingresar a: [Jboss](http://167.99.144.193:9990/) y seleccionar deployments_


## Herramientas utilizadas

* [Bitbucket](https://bitbucket.org/curso-mitocodews/app-monolith-covid19) - Repositorio de código fuente
* [Jenkins](http://208.68.39.62:8080/) - Servidor de automatización
* [Sonarqube](http://208.68.39.62:9000/projects) - Herramienta para evaluar código fuente
* [JFrog Artifactory](http://208.68.39.62:8082/ui/) - Repositorio de artefactos universal
* [Jboss](http://167.99.144.193:9990/) - Servidor de aplicaciones Java EE
* [Maven](https://maven.apache.org/) - Herramienta de software para la gestión y construcción de proyectos
* [Docker](https://www.docker.com/) - Automatiza el despliegue de aplicaciones dentro de contenedores de software
* [JaCoCo](https://www.jacoco.org/jacoco/trunk/doc/) - Plugin de maven que se utiliza para realizar reportes
* [Git](https://git-scm.com/) - Software de control de versiones
* [Junit4](https://junit.org/junit4/) - herramienta para pruebas unitarias


## Autor
* **Gilmar Lam** - *Alumno* - [gilmarlam](https://bitbucket.org/gilmarlam/) 
 
 
