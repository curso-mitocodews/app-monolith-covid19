package com.glam.covid19.service;

import java.util.List;
import java.util.Optional; 

import com.glam.covid19.model.Informacion;

public interface InformacionService {

	List<Informacion> findAll();
	Optional<Informacion> findById(Integer id);	
	Informacion save(Informacion t);
	
}
