package com.glam.covid19.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
 
import com.glam.covid19.model.Informacion;
import com.glam.covid19.service.InformacionService;

@RestController
@RequestMapping(value = "/info", produces = MediaType.APPLICATION_JSON_VALUE)
public class InformacionController {

	@Autowired
	private InformacionService informacionService;

	
	@GetMapping() 
	public ResponseEntity<List<Informacion>> findAll() {
		return ResponseEntity.ok(informacionService.findAll());
	}
	

	@GetMapping("/{id}") 
	public ResponseEntity<Informacion> findById(@PathVariable Integer id) {
		Optional<Informacion> list = informacionService.findById(id);
	   return ResponseEntity.ok(list.get()); 

	}
	
		
	@PostMapping() 
	public ResponseEntity<Object> insertar(@RequestBody Informacion info) {
		Informacion informacion = informacionService.save(info);
		
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(informacion.getId()).toUri();
		
		return ResponseEntity.created(location).build();

	} 
	
	

}
